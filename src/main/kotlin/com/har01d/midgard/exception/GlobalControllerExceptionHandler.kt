package com.har01d.midgard.exception

import org.springframework.boot.web.servlet.error.ErrorAttributes
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.ServletWebRequest
import javax.persistence.EntityNotFoundException
import javax.servlet.http.HttpServletRequest

@ControllerAdvice
class GlobalControllerExceptionHandler(val errorAttributes: ErrorAttributes) {

    @ExceptionHandler(EntityNotFoundException::class)
    fun notFound(request: HttpServletRequest): ResponseEntity<Map<String, Any>> {
        request.setAttribute("javax.servlet.error.status_code", HttpStatus.NOT_FOUND.value())
        val webRequest = ServletWebRequest(request)
        val body = errorAttributes.getErrorAttributes(webRequest, false)

        return ResponseEntity(body, HttpStatus.NOT_FOUND)
    }

}
