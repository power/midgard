package com.har01d.midgard.group

import org.springframework.data.annotation.CreatedDate
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "`group`")
data class Group(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Int = 0,
    @Column(nullable = false, unique = true, length = 35) val name: String,
    @Column(columnDefinition = "TEXT") var description: String,
    var cover: String = "",
    @Enumerated(EnumType.STRING) @Column(length = 15) var access: GroupAccess = GroupAccess.PUBLIC
) {

    @CreatedDate
    val createdTime = Date()

}

data class GroupDto(val name: String, val description: String, val cover: String = "")

enum class GroupAccess {
    PUBLIC,
    MEMBER,
    ADMIN,
    PRIVATE,
}
