package com.har01d.midgard.group

import com.har01d.midgard.exception.BadRequestException
import com.har01d.midgard.user.User
import com.har01d.midgard.user.UserRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

interface GroupService {
    fun createGroup(groupDto: GroupDto): Group
    fun getGroupMembers(groupId: Int, pageable: Pageable): Page<GroupMemberVo>
    fun addGroupMembers(groupId: Int, members: Array<Int>)
    fun getGroupMember(groupId: Int, userId: Int): GroupMember?
    fun addGroupMember(group: Group, user: User, type: GroupMemberType): GroupMember
    fun isGroupMemberExist(groupId: Int, userId: Int): Boolean
    fun setGroupMemberType(group: Group, user: User, type: GroupMemberType): GroupMember
}

@Service
class GroupServiceImpl(
    val groupRepository: GroupRepository,
    val userRepository: UserRepository,
    val groupMemberRepository: GroupMemberRepository
) : GroupService {

    override fun createGroup(groupDto: GroupDto): Group {
        var group = Group(
            name = groupDto.name,
            description = groupDto.description,
            cover = groupDto.cover
        )
        group = groupRepository.save(group)

        return group
    }

    override fun getGroupMembers(groupId: Int, pageable: Pageable): Page<GroupMemberVo> {
        return groupMemberRepository.findByGroupId(groupId, pageable)
            .map { GroupMemberVo(it.group.id, it.group.name, it.user.id, it.user.name, it.type, it.joinTime) }
    }

    override fun addGroupMember(group: Group, user: User, type: GroupMemberType): GroupMember {
        if (isGroupMemberExist(group.id, user.id)) {
            throw BadRequestException("Group member already exist!")
        } else {
            val member = GroupMember(group = group, user = user, type = type)
            return groupMemberRepository.save(member)
        }
    }

    override fun setGroupMemberType(group: Group, user: User, type: GroupMemberType): GroupMember {
        var member = getGroupMember(group.id, user.id)
        if (member == null) {
            member = GroupMember(group = group, user = user, type = type)
        } else {
            member.type = type
        }
        return groupMemberRepository.save(member)
    }

    override fun addGroupMembers(groupId: Int, members: Array<Int>) {
        val group = groupRepository.getOne(groupId)
        for (userId in members) {
            if (userRepository.existsById(userId)) {
                if (!isGroupMemberExist(groupId, userId)) {
                    val user = userRepository.getOne(userId)
                    val member = GroupMember(group = group, user = user)

                    groupMemberRepository.save(member)
                } else {
                    // group member exist
                }
            } else {
                // user doesn't exist
            }
        }
    }

    override fun isGroupMemberExist(groupId: Int, userId: Int) =
        groupMemberRepository.existsByGroupIdAndUserId(groupId, userId)

    override fun getGroupMember(groupId: Int, userId: Int) =
        groupMemberRepository.findByGroupIdAndUserId(groupId, userId)

}
