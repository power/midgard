package com.har01d.midgard.group

import com.har01d.midgard.user.User
import org.springframework.data.annotation.CreatedDate
import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity
data class GroupMember(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Int = 0,
    @ManyToOne val group: Group,
    @ManyToOne val user: User,
    @Enumerated(EnumType.STRING) @Column(length = 15) var type: GroupMemberType = GroupMemberType.USER
) : Serializable {

    @CreatedDate
    val joinTime = Date()

}

data class GroupMemberVo(
    val groupId: Int,
    val groupName: String,
    val userId: Int,
    val username: String,
    val type: GroupMemberType,
    val joinTime: Date
)

enum class GroupMemberType {
    CREATOR,
    ROOT,
    ADMIN,
    USER
}
