package com.har01d.midgard.group

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository

interface GroupRepository : JpaRepository<Group, Int>

interface GroupMemberRepository : JpaRepository<GroupMember, Int> {
    fun existsByGroupIdAndUserId(groupId: Int, userId: Int): Boolean
    fun findByGroupIdAndUserId(groupId: Int, userId: Int): GroupMember?
    fun findByGroupId(groupId: Int, pageable: Pageable): Page<GroupMember>
}
