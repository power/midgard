package com.har01d.midgard.group

import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/groups")
class GroupController(val groupRepository: GroupRepository,
                      val groupService: GroupService
) {

    @GetMapping("")
    fun getGroups(pageable: Pageable) = groupRepository.findAll(pageable)

    @GetMapping("/{id}")
    fun getGroupById(@PathVariable id: Int) = groupRepository.getOne(id)

    @GetMapping("/{id}/members")
    fun getGroupMembers(@PathVariable id: Int, pageable: Pageable) = groupService.getGroupMembers(id, pageable)

    @PostMapping("/{id}/members")
    fun addGroupMembers(@PathVariable id: Int, @RequestBody members: Array<Int>) {
        groupService.addGroupMembers(id, members)
    }

}
