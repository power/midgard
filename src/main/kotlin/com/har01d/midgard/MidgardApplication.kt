package com.har01d.midgard

import com.har01d.midgard.group.*
import com.har01d.midgard.user.User
import com.har01d.midgard.user.UserRepository
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@EnableJpaRepositories
@SpringBootApplication
class MidgardApplication(
    val userRepository: UserRepository,
    val groupRepository: GroupRepository,
    val groupService: GroupService,
    val groupMemberRepository: GroupMemberRepository
) : CommandLineRunner {

    override fun run(vararg args: String?) {
        val adminGroup = groupService.createGroup(
            GroupDto(
                name = "阿斯嘉特",
                description = "神国。阿萨神族居住的地方。"
            )
        )

        val admin = User(name = "admin")
        userRepository.save(admin)

        groupService.addGroupMember(adminGroup, admin, GroupMemberType.CREATOR)

        val user = User(name = "user")
        userRepository.save(user)

        val userGroup = groupService.createGroup(
            GroupDto(
                name = "米德加尔特",
                description = "人类居住的地方。这里有由冰、火、空气构成的三色彩虹桥（Bifrost），可以通往神国。"
            )
        )

        groupService.createGroup(
            GroupDto(
                name = "赫尔海姆",
                description = "这是一个冰冷多雾的地方，也是个永夜的场所，只有亡者才能到达。"
            )
        )

        groupService.createGroup(
            GroupDto(
                name = "尼福尔海姆",
                description = "冰之国。和死亡国没有明显分别的冰雪世界。"
            )
        )

        groupService.addGroupMember(userGroup, user, GroupMemberType.CREATOR)

        for (i in 1..1000) {
            addUser("test-$i", userGroup)
        }

        println(groupRepository.findAll())
        println(userRepository.findAll())
        println(groupService.isGroupMemberExist(1, 1))
        println(groupService.getGroupMember(1, 1))
        println(groupService.getGroupMember(1, 2))
    }

    private fun addUser(name: String, group: Group? = null) {
        val user = User(name = name)
        userRepository.save(user)

        if (group != null) {
            addGroupMember(group, user)
        }
    }

    private fun addGroupMember(group: Group, user: User) {
        val member = GroupMember(group = group, user = user)
        groupMemberRepository.save(member)
    }
}

fun main(args: Array<String>) {
    SpringApplication.run(MidgardApplication::class.java, *args)
}
