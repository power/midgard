package com.har01d.midgard.task

import com.fasterxml.jackson.annotation.JsonIgnore
import com.har01d.midgard.user.User
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import java.util.*
import javax.persistence.*

@Entity
data class Task(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Int = 0,
    @Column(nullable = false) var name: String,
    var content: String,
    @ManyToOne @JoinColumn val author: User,
    var startTime: Date = Date(),
    var endTime: Date? = null,
    @Enumerated(EnumType.STRING) @Column(length = 15) var recurrence: TaskRecurrence = TaskRecurrence.DAILY,
    @Enumerated(EnumType.STRING) @Column(length = 15) var access: TaskAccess = TaskAccess.PUBLIC_ACCESS
) {

    @CreatedDate
    val createdTime = Date()

    @JsonIgnore
    @LastModifiedDate
    var lastModifiedTime: Date? = null

}

enum class TaskRecurrence {
    ONCE,
    DAILY,
    WEEKLY,
    MONTHLY,
    UNLIMITED
}

enum class TaskAccess {
    PUBLIC_ACCESS,
    MEMBER_ACCESS,
}
