package com.har01d.midgard.user

import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/users")
class UserController(val userRepository: UserRepository) {

    @GetMapping("")
    fun getUsers(pageable: Pageable) = userRepository.findAll(pageable)

    @GetMapping("/{id}")
    fun getUserById(@PathVariable id: Int) = userRepository.getOne(id)

}
