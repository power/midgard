package com.har01d.midgard.user

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import java.util.*
import javax.persistence.*

@Entity
data class User(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Int = 0,
    @Column(nullable = false, unique = true, length = 35) val name: String,
    @Column(unique = true, length = 35) var email: String? = null,
    @Column(unique = true, length = 20) var mobile: String? = null,
    @Column(unique = true, length = 20) var qq: String? = null,
    @Column(unique = true, length = 35) var wechat: String? = null,
    @Column(unique = true, length = 35) var weibo: String? = null
) {
    var enabled = false

    var credentialsExpired = false

    var expired = false

    var locked = false

    @CreatedDate
    val createdTime = Date()

    @JsonIgnore
    @LastModifiedDate
    var lastModifiedTime: Date? = null

    @JsonIgnore
    var lastLoginTime: Date? = null
}
